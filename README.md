# Приложение для тестирования школьников на знание английского языка под C# WPF
Логика тестирования скопирована у конструктора тестов easyQuizzy, делалось для курсового проекта в [колледже](https://github.com/GeorgiyDemo/KIP)

#### Пакеты Nuget
- PDFsharp-MigraDoc - красивый вывод отчетов в PDF
- Microsoft.Data.Sqlite - взаимодействие с Sqlite

#### Скриншоты
<p align="center">
    <img src="https://github.com/GeorgiyDemo/TrainingEng/blob/img/img1.png"  width="740" height="429"/>
</p>

<p align="center">
    <img src="https://github.com/GeorgiyDemo/TrainingEng/blob/img/img2.png"  width="740" height="429"/>
</p>

<p align="center">
    <img src="https://github.com/GeorgiyDemo/TrainingEng/blob/img/img3.png"  width="740" height="429"/>
</p>

<p align="center">
    <img src="https://github.com/GeorgiyDemo/TrainingEng/blob/img/img4.png"  width="740" height="429"/>
</p>